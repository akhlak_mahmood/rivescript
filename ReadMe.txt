This is a RiveScript parser library for creating chatbot based on RiveScript.
This is actually a fork of RiveScript 2.0 with many additional features for easier scripting.

The following extra features are currently added:

	# The next discussion topic can be set using either "T(topic name)" or "Tx(topic name)". The
	First one will echo, while the latter will not. All spaces will be converted to underscores.
	So the real topic definition should use underscores, e.g. "> topic topic_name"
	This is easier alternative to "{topic=topic name}".

	# "{random}hi|hello{/random}" has two new brothers: "{rand}hi|hello{/rand}" and "r,,hi|hello/r"
	We wanted easier random keyword.

	# We can now call python object with the syntax "[call:obj_name arg1 arg2 etc]",
	whereas the default "<call>obj_name arg1 arg2 etc</call>" is still supported.

	# <*N> can now be used in place of <starN>

	# Conditional topic selection
    Select topic depending on number of times the topic was discussed before.
    The topic should be defined as "> topic topic_name:n", where n denotes the no of times the topic
    was discussed before. For example "> topic topic_name:3" will match only if this topic was discussed
    3 or more times before. However, if this conditional selection is already handled by a higher value
    say, "> topic topic_name:4" the previous lower one will be ignored.

	# We can now write several conditions on a single reply.
	All conditions must be true (like AND) if the reply is to be selected. Original RiveScript implementation lets us
	set only one condition. However, the or option (OR) can be implemented easily even with the original implementation.
	The syntax for AND condition is: "left eq right=> left eq right=> left eq right=> left eq right=> reply string"

	# The undefined variables can now be compared with number. In case of comparison, the value of the variable is
	set to zero. For example, "* <get age> < 18 => Sorry, you are not allowed here!" will work even if the age had not
	been set before.
	Original RiveScript implementation raises exception in case of this kind of comparisons.
	
	# RiveScript now stores a user variable "_trigMatched" containing the number of times the current trigger was matched before.
	So, "<get _trigMatched>" can be used in the conditionals. But, "*" and "request" of the > begin block won't be 
	considered in this case. Triggers of different topics are considered separately.
	
	# When a object is called its returned value is stored in the user variable "_lastResult" so that same object
	does not need to be called again, "<get _lastResult>" can be used instead.
		
	# Another user variable "_prevTopic" is set as the previous topic. Which can be retrieved using <get _prevTopic>

	# Changed "undefined" to "None" and "<undef>" to "<None>"
	
	# The RiveScript can now declare subjects and use them.
	Each directory will be considered as a subject.
	Added <load subjectName>, <unload subjectName>, <subject subjectName> tags.
	However the <subject> tag automattically loads the subject if not already loaded. Hence normally
	<subject> tag should be used, not <load>. However, depending on the situation you can load unload it even without
	selecting it. This can be useful if the Subject is huge in which cage it will take time to load a subject and
	parse it.
	Also <subject Parent/Child> can be used to load the subject "Child" located inside the "Parent" subject directory.
	However, loading Parent will not automatically load the nested Child subjects.
	Once a subject is loaded, the scope is inside that subject only. Hence even if the Child is loaded, the triggers of
	Parent or Default will be out of scope. This can be overcome using importing.
	The default subject is "def."

	# Arrays should now be defined in files, not in the begin block.

	# All arrays, topics and objects local to a subject are automattically added under the subject.
	They are stored as SubjectNameArrayName, SubjectNameTopicName and SubjectNameObjectName.
	It is user's responsibility to add a dot (.) after the subject name.

	# Added SetDebug(bool) method to the RiveScript class.

	# The default Subject topic is random. So it is "subjectName.random". When a subject is loaded and the 
	topic is automattically set to random. But, you can set to a specific topic by combining the subject and 
	set topic tags. For example, - <subject Apple> Tx(Taste) {@<input>}

	# The hash (#) is used for importing now, so it will no longer be used for comments.
	It can be used to import another file from another subject like "# import subject.file_name"
	Additionally "# import this.file_name" can also be used to import a file within the current directory.
	This is handy if the imported file depends on another file inside that subject.
	Importing of default subject in not allowed.
	Also, "<import subject.file>" tag will behave just like the # import preprocessor. This will allow conditional
	importing on runtime.
	If a file is imported, its triggers are inside the scope. So the triggers will match.

	# While importing or laoding from a nested subject directory, "/" needs to be used, not ".", otherwise it'll fail to
	find the file.

	# Topic specific static variables can be set and get just like the user vars.
	In this case, <setv> and <getv> is to be used. The topic vars will be subject specific, so if the same
	topic is imported by different subjects, the variables will be different for them. And the vars will not be
	deleted on subject unload.
	They are more like topic's local static variables, they are not discarded even after topic is exited.
	These vars can be further controlled via <addv> <subv> <multv> and <divv> tags.

	# Added some extra shortcut tags <@i>, <@r>, <@i1>, <@r1>, <@i2>, <@r2>, <@i3>, <@r3> for {@<input>}, {@<reply>}
	{@<input1>}, {@<reply1>}, {@<input2>}, {@<reply2>}, {@<input3>}, {@<reply3>} respectively.


################################################
################################################
The main problem with RiveScript's current implementation is that every possible
user inputs and replies must be explicitly defined in the scripts. Otherwise the bot
will not be able to generate any answer. Which means, basically it is just a
dumb clark, doesn't have any kind of "intelligence" built in.

However this could be improved if a simplest machine learning could be implemented.
But the problem is to find the right context and generate the best process
to learn something new. Which is essentially the problem of mordern AI.

For example like the Google's bot, we could input some movie scripts inside the
bots brain and generate reply from it. But that too is essentially the same thing.
Not a true "intelligence".

Now the question is if its even possible to build something "intelligent". If so,
how? At present, we are only able to mimic the intelligence. We are not yet
able to develop something who has intelligence of its own.

To overcome this problem we first need to understand what intelligence is. In 
the simplest sense it could be reasoning and making the correct decision. In 
that case we can reach our goal only by mimicking the intelligence. But the
best would be learning something new, analyzing it and use it for
future events. Only that can be regarded as the true intelligence.

Every intelligent being actually follows this simple pattern. Whenever they encounter
something new they learn from it and use it for future reference. They too
mimic the events of the past situations to solve the latter ones. We, humans, too
use this process. The difference is mainly we are very fast learner and we
have the most powerful analyzing and decision making power.

To develop a successful AI, we need to follow this path to achieve an acceptable
intelligence. To do so we need a machine that can analyze the new situations
and has the capability of making decision. Can this only be implemented using
just mathematics? Well, it should be. The reason is everything can be achieved
using pure mathematics.

So the primary concern is to analyze something new. But how?
Humans have ability to match any pattern. We immediately recognize new things
by comparing it with something we already familiar with.

Natural language processing is by far one of the toughest jobs for AI.
Even we, humans need a large amount of processing power of our brains
to achieve the ability of using our mother toung. And we need almost 4/5
years to be competent and fluent. So jumping right into the NLP to develop
a good AI is a fools work. There would be mountains of works to do and
we will have no idea if we are making any advancements or not.
Therefore we should actually start with something easier but equally challenging.

ANN - Artificial Neural Networks have been a primary interest of mine for some
time. But even after mimicking the neurons we are not very much able to perform
the complex tasks. Perhaps we should make something of our own.

I have an idea to make a protozoa type being. Just finding food and living its life.
Though this kind of AI have already been developped and perhaps we have already
seen them in video games or somewhere else, we really need to start from the scratch.

Its like the game of virtual villegers. But I would really like to make one of my own,
play with their food, shelter, emotions, basically play God. Which would be awesome.

One thing I thought previously. The emotions, at the most basic level, can be divided in
only two categories. Pain and pleasure. We only avoid pains to attain the pleasures.
Perhaps this little virtual being of mine will have only this two emotions.

Human brain is the most extraordinary thing in the entire universe. Only requires about twenty
watts of power but does the umimaginable amount of work. We perhaps a light years behind
achieving anything remotely similiar. But starting from the tiniest bit can be equally exciting
too.