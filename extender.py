# This module maintains several stacks of messages, replies etc to extend the functionalities of
# RiveScript. Added by Akhlak Mahmood

class stack(object):
    def __init__(self, n = 100):
        self.count = n
        self.items = []

    def add(self, i):
        self.items.append(i)
        if len(self.items) > self.count:
            self.pop()
        
    def pop(self):
        temp = items
        self = items[1:]
        return temp[0]
        
    def len(self):
        return len(items)
        
    def seek(self, n):
        return items[n]
        
    def last(self):
        return items[-1]
        
    def contain(self, a):
        return self.items.count(a)
        
    def getStack(self):
        return self.items
        
    def clear(self):
        self.items = []
        
    def getAt(self, index):
        return self.items[index]

Replies = stack(30)
Messages = stack(30)
FormattedMessages = stack(30)
Topics = stack(100)
Triggers = stack(1000)

Dict = {"0":"zero"}

User = None

def reInitialize():
    global Replies, Messages, FormattedMessages, Topics, Triggers
    Replies = stack(30)
    Messages = stack(30)
    FormattedMessages = stack(30)
    Topics = stack(100)
    Triggers = stack(1000)
    