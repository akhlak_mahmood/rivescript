""" This file is an example of running the RiveScript bot.
	The file should be in the parent directory of RiveScript module.
"""

import os
import sys

os.chdir("..")
sys.path.append(os.getcwd())

from RiveScript import rivescript as rive

bot = rive.RiveScript()

bot.load_directory("./RiveScript/brain sample")
bot.sort_replies()

while True:
    msg = input('You> ')

    if msg == '/quit':
        break
    
    elif msg == '/debug':
    	bot.SetDebug(True)

    elif msg == '/nodebug':
    	bot.SetDebug(False)

    elif msg == '/reload':
        bot = rive.RiveScript()
        bot.load_directory("./RiveScript/brain sample")
        bot.sort_replies()

    else:
        reply = bot.reply("localuser", msg)
        print('Bot> ' + reply)

print("Goodbye!")
